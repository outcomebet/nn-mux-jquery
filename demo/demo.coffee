$ ->

  showResult = (res, data)->
    $('#result').html( $('#result').html() + "\n\n#{res}:\n" + JSON.stringify(data) )

  showOkResult = (data)->
    showResult 'ok', data

  showFailResult = (res)->
    showResult 'fail', res.message


  # Create API client
  window.mux = mux = new NnMux
    # Define actions
    actions:
      # Preferred way
      api: ['version', 'sleep']
      auth: ['state', 'login', 'logout']

      # Alternative way (useful when a namespace contains both actions and nested namespaces)
      balance:
        get: 1

      # Nested namespaces
      # This action don't exist on server (used for testing error handler)
      the:
        great:
          cornholio: 1
          other: ['stuff']


  # Missing method
  $('#button-0').on 'click', ->
    mux.the.great.cornholio()
    .then showOkResult
    .fail showFailResult

  # Get current user state
  $('#button-1').on 'click', ->
    mux.auth.state()
    .then (user)->
      if (user)
        showOkResult user.login
      else
        showOkResult 'Not logged in!'

  # User log in
  $('#button-2').on 'click', ->
    mux.auth.login
      login: $('#login').val()
      password: $('#password').val()
    .then showOkResult
    .fail showFailResult

  # User log out
  $('#button-3').on 'click', ->
    mux.auth.logout()
    .then showOkResult

  # Chaining promises
  $('#button-4').on 'click', ->
    mux.auth.login
      login: $('#login').val()
      password: $('#password').val()

    .then (result)->
      showResult 'login', result
      if result && result.value
        mux.auth.state()
      else
        jQuery.Deferred().reject(message: 'Auth failed').promise()

    .then (user)->
      showResult 'user', user.login
      mux.balance.get()

    .then (balance)->
      showResult 'balance', balance.value

    .fail showFailResult

  # Debouncing requests
  $('#button-5').on 'click', ->
    mux.balance.get().then(showOkResult).fail(showFailResult)

    setTimeout ->
      mux.balance.get().then(showOkResult).fail(showFailResult)
    , 50

    setTimeout ->
      mux.balance.get().then(showOkResult).fail(showFailResult)
    , 100


  # Multi callbacks
  $('#button-6').on 'click', ->
    promise = mux.balance.get()

    promise.then (balance)->
      showResult 'cb1', balance
      'err'

    promise.then (balance)->
      showResult 'cb2', balance


  $('#button-7').on 'click', ->
    mux.api.version()
    .then showOkResult
    .fail showFailResult

  $('#button-8').on 'click', ->
    mux.api.sleep(duration: 2000)
    .then showOkResult
    .fail showFailResult

  0
