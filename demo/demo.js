$(function() {
  var mux, showFailResult, showOkResult, showResult;
  showResult = function(res, data) {
    return $('#result').html($('#result').html() + ("\n\n" + res + ":\n") + JSON.stringify(data));
  };
  showOkResult = function(data) {
    return showResult('ok', data);
  };
  showFailResult = function(res) {
    return showResult('fail', res.message);
  };
  window.mux = mux = new NnMux({
    actions: {
      api: ['version', 'sleep'],
      auth: ['state', 'login', 'logout'],
      balance: {
        get: 1
      },
      the: {
        great: {
          cornholio: 1,
          other: ['stuff']
        }
      }
    }
  });
  $('#button-0').on('click', function() {
    return mux.the.great.cornholio().then(showOkResult).fail(showFailResult);
  });
  $('#button-1').on('click', function() {
    return mux.auth.state().then(function(user) {
      if (user) {
        return showOkResult(user.login);
      } else {
        return showOkResult('Not logged in!');
      }
    });
  });
  $('#button-2').on('click', function() {
    return mux.auth.login({
      login: $('#login').val(),
      password: $('#password').val()
    }).then(showOkResult).fail(showFailResult);
  });
  $('#button-3').on('click', function() {
    return mux.auth.logout().then(showOkResult);
  });
  $('#button-4').on('click', function() {
    return mux.auth.login({
      login: $('#login').val(),
      password: $('#password').val()
    }).then(function(result) {
      showResult('login', result);
      if (result && result.value) {
        return mux.auth.state();
      } else {
        return jQuery.Deferred().reject({
          message: 'Auth failed'
        }).promise();
      }
    }).then(function(user) {
      showResult('user', user.login);
      return mux.balance.get();
    }).then(function(balance) {
      return showResult('balance', balance.value);
    }).fail(showFailResult);
  });
  $('#button-5').on('click', function() {
    mux.balance.get().then(showOkResult).fail(showFailResult);
    setTimeout(function() {
      return mux.balance.get().then(showOkResult).fail(showFailResult);
    }, 50);
    return setTimeout(function() {
      return mux.balance.get().then(showOkResult).fail(showFailResult);
    }, 100);
  });
  $('#button-6').on('click', function() {
    var promise;
    promise = mux.balance.get();
    promise.then(function(balance) {
      showResult('cb1', balance);
      return 'err';
    });
    return promise.then(function(balance) {
      return showResult('cb2', balance);
    });
  });
  $('#button-7').on('click', function() {
    return mux.api.version().then(showOkResult).fail(showFailResult);
  });
  $('#button-8').on('click', function() {
    return mux.api.sleep({
      duration: 2000
    }).then(showOkResult).fail(showFailResult);
  });
  return 0;
});
