gulp = require 'gulp'
coffee = require 'gulp-coffee'
rename = require 'gulp-rename'
minify = require 'gulp-uglify'


gulp.task 'default', ->
  console.log 'Hello world!'

gulp.task 'compile', ->
  gulp.src 'nn-mux-jquery.coffee'
  .pipe coffee bare: true, header: true
  .pipe gulp.dest 'dist/'
  .pipe minify()
  .pipe rename 'nn-mux-jquery.min.js'
  .pipe gulp.dest 'dist/'

gulp.task 'compile-demo', ->
  gulp.src 'demo/demo.coffee'
  .pipe coffee {bare: true}
  .pipe gulp.dest 'demo/'

gulp.task 'watch', ->
  gulp.watch 'nn-mux-jquery.coffee', ['compile']

gulp.task 'watch-demo', ->
  gulp.watch 'demo/demo.coffee', ['compile-demo']


gulp.task 'default', ['compile', 'compile-demo', 'watch', 'watch-demo']
