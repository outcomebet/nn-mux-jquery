# NnMux API Client #

## Requirements ##
jQuery >= 1.5.1
(Uses Ajax, Deferred & Utilities modules)

## Start ##
Go to your console and navigate to `www-data` directory of your HTTP server and run:
```
git clone git@bitbucket.org:outcomebet/nn-mux-jquery.git
```

## Demo ##
* Run demo server `demo/server/gomuxer_{YOUR_ARCH}` for your system
* The server doesn't support CORS. Therefore you need to passthrough (proxify) the server to your webserver.
  Example of config section for nginx:

```
location /demux/handler {
  proxy_pass http://localhost:3000/api;
}
```

* Open `http://localhost/nn-mux-jquery/demo/index.html` in your browser.


## Development ##
* Source code located in `nn-mux-jquery.coffee`
* Compiled JS and minified versions located in `dist/`
* 
* If you want to edit source code, you need to install `npm`, then install `gulp`:

```
npm install
sudo npm install -g gulp
```

* Compile:

```
gulp
```

This will execute all of the following tasks: `compile`, `compile-demo`, `watch`, `watch-demo`.

* Or you can manually run any one of them like this:

```
gulp compile
```

## Documentation ##
* See usage documentation in `nn-mux-jquery.coffee` or examples in `demo/demo.coffee`



## Production ##
### Using npm ###
* To install required modules run in console:

```
npm install --production
```

* Add scripts to your html:
```
node_modules/jquery/dist/jquery.min.js
dist/nn-mux-jquery.min.js
```

### Using bower ###
* To install required modules run in console:
```
bower install
```
* Add scripts to your html:
```
bower_components/jquery/dist/jquery.min.js
dist/nn-mux-jquery.min.js
```

## Questions ##
darkserg@gmail.com
