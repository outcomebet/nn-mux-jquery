# NnMux API Client
# requires: jquery >= 1.5.1
#
# Usage
# options = {
#   url: 'https://site.com/api',
#   actions: {
#     user: ['auth']
#   }
# };
# mux = new NnMux(options);
# mux.user.auth().then(dance).fail(cry);
#
# options:
#   [url] | [[protocol] [hostname] [port] [path]]
#     Define URL of demux server.
#     By default generates url from current location (path: '/demux/handler')
#
#   [debounce]
#     Debouncing requests (default: 200ms)
#
#   [xhrOptions]
#     Options to pass to XHR object
#     [withCredentials: true]
#       Enable cookies
#
#   [actions]
#     Methods of API



class NnMux
  @defaults:
    protocol: location.protocol
    hostname: location.hostname
    port: location.port
    path: '/demux/handler'
    url: null
    debounce: 200
    actions: null
    xhrOptions:
      withCredentials: true

  constructor: (config)->
    opt = jQuery.extend {}, @constructor.defaults, config

    if !opt.url?
      port = if opt.port then (':' + opt.port) else ''
      opt.url = "#{opt.protocol}//#{opt.hostname}#{port}#{opt.path}"

    # console.log @constructor.defaults, config, opt


    # init queue and request methods
    queue = []

    queueAction = (action, args)->
      dfd = jQuery.Deferred()

      queue.push
        action: action
        arguments: args
        deferred: dfd

      #console.log 'queued', queue

      debouncedRequest()

      dfd.promise()

    doRequest = ->
      # preserve current queue in closure
      pendingQueue = queue
      # clean
      queue = []

      # generate request
      requestData = pendingQueue.map (req)->
        action: req.action
        arguments: req.arguments

      #console.log 'request data', requestData

      jQuery.ajax
        url: opt.url,
        method: 'POST',
        contentType: 'application/json'
        dataType: 'json'
        responseType: 'json'
        xhrFields: opt.xhrOptions
        data: JSON.stringify(requestData)

      .done (result, textStatus, jqXHR)->
        #console.log 'response', res, textStatus, jqXHR

        # parse result
        if jQuery.isArray result
          # ok
          for response in result
            request = pendingQueue.splice(0,1)[0]

            if response.success
              #console.log 'success', response
              request.deferred.resolve(response.data)
            else
              #console.log 'unsuccess', response
              request.deferred.reject(response)

        else
          # result is not array
          console.warn 'Response format error', result

        1

      .fail (jqXHR, textStatus, errorThrown)->
        console.warn 'Request error', errorThrown


    # Creates and returns a new debounced version of the passed function which will postpone its execution until
    # after wait milliseconds have elapsed since the last time it was invoked.
    # http://underscorejs.org/#debounce
    debounce = (callback) ->
      timeout = null
      previous = 0

      later = ->
        previous = 0
        timeout = null
        callback()
        return

      ->
        now = Date.now()
        if !previous
          previous = now
        remaining = opt.debounce - (now - previous)
        if remaining <= 0 or remaining > opt.debounce
          clearTimeout timeout
          timeout = null
          previous = now
          callback()
        else if !timeout
          timeout = setTimeout(later, remaining)
        return

    debouncedRequest = debounce doRequest

    # Generate namespace (plain object) from passed config object or array.
    # If target is passed it will be modified.
    genNs = (conf, prefix, target = {})->
      if jQuery.isArray conf
        for action in conf
          unless typeof action is 'string'
            throw 'Actions config error'
          target[action] = genAction prefix + action

      else if jQuery.isPlainObject conf
        for own ns, actions of conf
          if jQuery.isArray(actions) or jQuery.isPlainObject(actions)
            target[ns] = genNs actions, prefix + ns + '.'
          else
            # Scalar definition: {actionName: 1}
            target[ns] = genAction prefix + ns

      target

    # Returns function which will call the action
    genAction = (action)->
      (args)-> queueAction action, args


    # Init root namespace
    genNs opt.actions, '', this
